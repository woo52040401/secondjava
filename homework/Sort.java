package com.tgl.fileIO.homework;

import java.util.Collections;
import java.util.Comparator;
import java.util.List;

public class Sort {
	public List<Employee> sortID(List<Employee> list) {
		Collections.sort(list, new Comparator<Employee>() {
			public int compare(Employee e1, Employee e2) {
				if (e1.getId() > e2.getId()) {
					return 1;
				} else if (e1.getId() < e2.getId()) {
					return -1;
				} else {
					return 0;
				}
			}
		});
		return list;
	}

	public List<Employee> sortTall(List<Employee> list) {
		Collections.sort(list, new Comparator<Employee>() {
			public int compare(Employee e1, Employee e2) {
				if (e1.getTall() > e2.getTall()) {
					return 1;
				} else if (e1.getTall() < e2.getTall()) {
					return -1;
				} else {
					return 0;
				}
			}
		});
		return list;
	}

	public List<Employee> sortWeight(List<Employee> list) {
		Collections.sort(list, new Comparator<Employee>() {
			public int compare(Employee e1, Employee e2) {
				if (e1.getWeight() > e2.getWeight()) {
					return 1;
				} else if (e1.getWeight() < e2.getWeight()) {
					return -1;
				} else {
					return 0;
				}
			}
		});
		return list;
	}

	public List<Employee> sortEnName(List<Employee> list) {
		Collections.sort(list, new Comparator<Employee>() {
			public int compare(Employee e1, Employee e2) {
				return e1.getEnName().compareTo(e2.getEnName());
			}
		});
		return list;
	}

	public List<Employee> sortPhone(List<Employee> list) {
		Collections.sort(list, new Comparator<Employee>() {
			public int compare(Employee e1, Employee e2) {
				return e1.getPhone().compareTo(e2.getPhone());
			}
		});
		return list;
	}

	public List<Employee> sortEmail(List<Employee> list) {
		Collections.sort(list, new Comparator<Employee>() {
			public int compare(Employee e1, Employee e2) {
				return e1.getEmail().compareTo(e2.getEmail());
			}
		});
		return list;
	}

	public List<Employee> sortBMI(List<Employee> list) {
		Collections.sort(list, new Comparator<Employee>() {
			public int compare(Employee e1, Employee e2) {
				if (e1.getBmi() > e2.getBmi()) {
					return 1;
				} else if (e1.getBmi() < e2.getBmi()) {
					return -1;
				} else {
					return 0;
				}
			}
		});
		return list;
	}
}
