package com.tgl.fileIO.homework;

import java.util.List;

public class Delete {
	public int deleteByID(List<Employee> list , int id){
		for(int i = 0; i < list.size(); i++) {
			if(list.get(i).getId() == id) {
				list.remove(i);
			}
		}
		System.out.println("Afet delete list: " + list);
		return list.size();
	}
	public int deleteByEnName(List<Employee> list , String name) {
		for(int i = 0; i < list.size(); i++) {
			if(list.get(i).getEnName().compareTo(name) == 0) {
				list.remove(i);
			}
		}
		System.out.println("Afet delete list: " +list);
		return list.size();
	}
	public int deleteByPhone(List<Employee> list , String phone) {
		for(int i = 0; i < list.size(); i++) {
			if(list.get(i).getPhone().compareTo(phone) == 0) {
				list.remove(i);
			}
		}
		System.out.println("Afet delete list: " +list);
		return list.size();
	}
}
