package com.tgl.fileIO.homework;

import java.util.List;

public class Search{
	
	public Employee searchID(List<Employee> list,int id){
		int i;
		for(i = 0; i < list.size(); i++) {
			if(list.get(i).getId() == id) {
				return list.get(i);
			}
		}
		return null;
	}
	public Employee searchEnName(List<Employee> list, String name) {
		int i;
		for(i = 0; i < list.size(); i++) {
			if(list.get(i).getEnName().compareTo(name) == 0) {
				return list.get(i);
			}
		}
		return null;
	}
	public Employee searchName(List<Employee> list, String name){
		int i;
		for(i = 0; i < list.size(); i++) {
			if(list.get(i).getName().compareTo(name) == 0) {
				return list.get(i);
			}
		}
		return null;
	}
	public Employee searchPhone(List<Employee> list ,String phone) {
		int i;
		for(i = 0; i < list.size(); i++) {
			if(list.get(i).getPhone().compareTo(phone) == 0) {
				return list.get(i);
			}
		}
		return null;
	}
	public Employee searchEmail(List<Employee> list, String email) {
		int i;
		for(i = 0; i < list.size(); i++) {
			if(list.get(i).getEmail().compareTo(email) == 0) {
				return list.get(i);
			}
		}
		return null;
	}
}
