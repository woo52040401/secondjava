package com.tgl.fileIO.homework;

import java.util.List;

public class MaxOrMin {
	public Employee Tall(List<Employee> list, String str){
		Sort sort = new Sort();
		sort.sortTall(list);
		if(str == "MAX") {
			return list.get(list.size()-1);
		}else if(str == "MIN") {
			return list.get(0);
		}
		return null;
	}
	
	public Employee Weight(List<Employee> list, String str) {
		Sort sort = new Sort();
		sort.sortWeight(list);
		if(str == "MAX") {
			return list.get(list.size()-1);
		}else if(str == "MIN") {
			return list.get(0);
		}
		return null;
	}
	
	public Employee BMI(List<Employee> list, String str) {
		Sort sort = new Sort();
		sort.sortBMI(list);
		if(str == "MAX") {
			return list.get(list.size()-1);
		}else if(str == "MIN") {
			return list.get(0);
		}
		return null;
	}
}
