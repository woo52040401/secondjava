package com.tgl.fileIO.homework;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class MainFunction {
	public Status status;

	public static void main(String[] args) {
		List<Employee> list = new ArrayList<>();
		String string;
		String[] strArray = null;
		int i = 1;
		float bmi;
		try (BufferedReader read = new BufferedReader(new FileReader("D:\\名單.txt"))) {
			while ((string = read.readLine()) != null) {
				strArray = string.split("\\s+");
				bmi = (float) (Integer.parseInt(strArray[1]) / Math.pow((Integer.parseInt(strArray[0]) * 0.01), 2));
				list.add(new Employee(i, Integer.parseInt(strArray[0]), Integer.parseInt(strArray[1]), strArray[2],
						strArray[3], strArray[4], strArray[5], bmi));
				i++;
			}
		} catch (IOException e) {
			e.printStackTrace();
		}
		//Search
		Search search = new Search();
		String str = "ID";
		
		switch (Status.valueOf(str)) {
		case ID:
			System.out.println("Search by ID: "+ search.searchID(list, 2));//第二筆
			break;
		case ENGLISHNAME:
			System.out.println("Search by English Name: "+ search.searchEnName(list, "Aaron-Huang"));//第二筆
			break;
		case NAME:
			System.out.println("Search by Name: "+ search.searchName(list, "黃大羚"));//第三筆
			break;
		case PHONE:
			System.out.println("Search by Phone: "+ search.searchPhone(list, "2115"));//第二筆
			break;
		case EMAIL:
			System.out.println("Search by Email: "+ search.searchEmail(list, "ayawong@transglobe.com.tw"));//第三筆
			break;
		default:
			System.out.println("Wrong!");
			break;
		}
		//Sort
		Sort sort = new Sort();
		String str1 = "TALL";
		switch (Status.valueOf(str1)) {
		case ID:
			System.out.println("Sort by ID: "+ sort.sortID(list));
			break;
		case TALL:
			System.out.println("Sort by Tall: "+ sort.sortTall(list));
			break;
		case WEIGHT:
			System.out.println("Sort by Weight: "+ sort.sortWeight(list));
			break;
		case ENGLISHNAME:
			System.out.println("Sort by English Name: "+ sort.sortEnName(list));
			break;
		case PHONE:
			System.out.println("Sort by Phone: "+ sort.sortPhone(list));
			break;
		case EMAIL:
			System.out.println("Sort by Email: "+ sort.sortEmail(list));
			break;
		case BMI:
			System.out.println("Sort by BMI: "+ sort.sortBMI(list));
			break;
		default:
			System.out.println("Wrong!");
			break;
		}

		// Create Data
		Create create = new Create();
		System.out.println("After Create size: " + create.createData(list, 58, 192, 50, "Yan", "李", "1234", "w123@transglobe.com.tw", (float) 19.21));

		// Delete Data
		Delete delete = new Delete();
		String str2 = "ENGLISHNAME";

		switch (Status.valueOf(str2)) {
		case ID:
			System.out.println("Delete By ID: "+delete.deleteByID(list, 1)); //第一筆
			break;
		case ENGLISHNAME:
			System.out.println("Delete By English Name: "+delete.deleteByEnName(list, "Aaron-Huang"));// 第二筆
			break;
		case PHONE:
			System.out.println("Delete By Phone: "+delete.deleteByPhone(list, "6144")); // 第三筆
			break;
		default:
			System.out.println("Wrong!");
			break;
		}

		// Max and Min 
		MaxOrMin mom = new MaxOrMin();
		String str3 = "BMI";
		String str4 = "MIN";

		switch (Status.valueOf(str3)) {
		case TALL:
			if (str4 == "MAX" || str4 == "MIN") {
				if(str4 == "MAX")
					System.out.println("Maximum Tall: " + mom.Tall(list, str4));
				else {
					System.out.println("Minimum Tall: " + mom.Tall(list, str4));
				}
			} else {
				System.out.println("Wrong");
			}
			break;
		case WEIGHT:
			if (str4 == "MAX" || str4 == "MIN") {
				if(str4 == "MAX")
					System.out.println("Maximum Weight: " + mom.Weight(list, str4));
				else {
					System.out.println("Minimum Weight: " + mom.Weight(list, str4));
				}
			} else {
				System.out.println("Wrong");
			}
			break;
		case BMI:
			if (str4 == "MAX" || str4 == "MIN") {
				if(str4 == "MAX")
					System.out.println("Maximum BMI: " + mom.BMI(list, str4));
				else {
					System.out.println("Minimum BMI: " + mom.BMI(list, str4));
				}
			} else {
				System.out.println("Wrong");
			}
			break;
		default:
			System.out.println("Wrong!");
			break;
		}
	}
}
